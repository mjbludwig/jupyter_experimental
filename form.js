'use strict'

/*
Morgan is well aware that this is some ugly JS but its in production...working...so he is going to let it stay this way...
*/

/*
  script to toggle anaconda install and environment options based on selection.
 code references lovingly stolen from https://github.com/OSC/bc_osc_ansys_workbench/blob/master/form.js
*/

function toggle_conda_install() {
  let conda_module = $('#batch_connect_session_context_conda_module');
  let conda_install = $('#batch_connect_session_context_conda_install');
  let conda_install_checkbox = document.getElementById("batch_connect_session_context_conda_install_checkbox"); 
if(conda_install_checkbox.checked === true)  {
   conda_install.parent().show();
   conda_module.parent().hide();
  } else {
   conda_install.parent().hide();
   conda_module.parent().show();
  }
}
function toggle_conda_env() {
  let conda_env = $('#batch_connect_session_context_conda_env');
  let conda_env_checkbox = document.getElementById("batch_connect_session_context_conda_env_checkbox"); 
if(conda_env_checkbox.checked === true)  {
   conda_env.parent().show();
  } else {
   conda_env.parent().hide();
  }
}

toggle_conda_install()
toggle_conda_env()
let install_trigger = $('#batch_connect_session_context_conda_install_checkbox');
install_trigger.change(toggle_conda_install);
let env_trigger = $('#batch_connect_session_context_conda_env_checkbox');
env_trigger.change(toggle_conda_env);
const form = document.getElementById("new_batch_connect_session_context");

form.addEventListener("submit", function(e) {
  let conda_env = document.getElementById("batch_connect_session_context_conda_env");
  let conda_install = document.getElementById("batch_connect_session_context_conda_install");
  if($('#batch_connect_session_context_conda_install').is(":visible") && conda_install.value === "") {
    alert("Custom conda install field cannot be blank!");
    e.preventDefault();
  }
  if($('#batch_connect_session_context_conda_env').is(":visible") && conda_env.value === "") {
    alert("Custom conda env field cannot be blank!");
    e.preventDefault();
  }

});
