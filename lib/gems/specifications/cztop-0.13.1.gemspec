# -*- encoding: utf-8 -*-
# stub: cztop 0.13.1 ruby lib

Gem::Specification.new do |s|
  s.name = "cztop".freeze
  s.version = "0.13.1"

  s.required_rubygems_version = Gem::Requirement.new(">= 0".freeze) if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib".freeze]
  s.authors = ["Patrik Wenger".freeze]
  s.bindir = "exe".freeze
  s.date = "2018-03-04"
  s.email = ["paddor@gmail.com".freeze]
  s.executables = ["z85decode".freeze, "z85encode".freeze]
  s.files = ["exe/z85decode".freeze, "exe/z85encode".freeze]
  s.homepage = "https://github.com/paddor/cztop".freeze
  s.licenses = ["ISC".freeze]
  s.rubygems_version = "2.7.7".freeze
  s.summary = "CZMQ Ruby binding, based on the generated low-level FFI bindings of CZMQ".freeze

  s.installed_by_version = "2.7.7" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_runtime_dependency(%q<czmq-ffi-gen>.freeze, ["~> 0.15.0"])
      s.add_development_dependency(%q<bundler>.freeze, ["~> 1.10"])
      s.add_development_dependency(%q<rake>.freeze, ["~> 10.0"])
      s.add_development_dependency(%q<rspec>.freeze, [">= 0"])
      s.add_development_dependency(%q<minitest>.freeze, [">= 0"])
      s.add_development_dependency(%q<rspec-given>.freeze, ["~> 3.8.0"])
      s.add_development_dependency(%q<pry>.freeze, [">= 0"])
      s.add_development_dependency(%q<yard>.freeze, [">= 0"])
      s.add_development_dependency(%q<guard>.freeze, [">= 0"])
      s.add_development_dependency(%q<guard-rspec>.freeze, [">= 0"])
      s.add_development_dependency(%q<guard-yard>.freeze, [">= 0"])
      s.add_development_dependency(%q<guard-shell>.freeze, [">= 0"])
      s.add_development_dependency(%q<terminal-notifier-guard>.freeze, [">= 0"])
      s.add_development_dependency(%q<foreman>.freeze, [">= 0"])
    else
      s.add_dependency(%q<czmq-ffi-gen>.freeze, ["~> 0.15.0"])
      s.add_dependency(%q<bundler>.freeze, ["~> 1.10"])
      s.add_dependency(%q<rake>.freeze, ["~> 10.0"])
      s.add_dependency(%q<rspec>.freeze, [">= 0"])
      s.add_dependency(%q<minitest>.freeze, [">= 0"])
      s.add_dependency(%q<rspec-given>.freeze, ["~> 3.8.0"])
      s.add_dependency(%q<pry>.freeze, [">= 0"])
      s.add_dependency(%q<yard>.freeze, [">= 0"])
      s.add_dependency(%q<guard>.freeze, [">= 0"])
      s.add_dependency(%q<guard-rspec>.freeze, [">= 0"])
      s.add_dependency(%q<guard-yard>.freeze, [">= 0"])
      s.add_dependency(%q<guard-shell>.freeze, [">= 0"])
      s.add_dependency(%q<terminal-notifier-guard>.freeze, [">= 0"])
      s.add_dependency(%q<foreman>.freeze, [">= 0"])
    end
  else
    s.add_dependency(%q<czmq-ffi-gen>.freeze, ["~> 0.15.0"])
    s.add_dependency(%q<bundler>.freeze, ["~> 1.10"])
    s.add_dependency(%q<rake>.freeze, ["~> 10.0"])
    s.add_dependency(%q<rspec>.freeze, [">= 0"])
    s.add_dependency(%q<minitest>.freeze, [">= 0"])
    s.add_dependency(%q<rspec-given>.freeze, ["~> 3.8.0"])
    s.add_dependency(%q<pry>.freeze, [">= 0"])
    s.add_dependency(%q<yard>.freeze, [">= 0"])
    s.add_dependency(%q<guard>.freeze, [">= 0"])
    s.add_dependency(%q<guard-rspec>.freeze, [">= 0"])
    s.add_dependency(%q<guard-yard>.freeze, [">= 0"])
    s.add_dependency(%q<guard-shell>.freeze, [">= 0"])
    s.add_dependency(%q<terminal-notifier-guard>.freeze, [">= 0"])
    s.add_dependency(%q<foreman>.freeze, [">= 0"])
  end
end
