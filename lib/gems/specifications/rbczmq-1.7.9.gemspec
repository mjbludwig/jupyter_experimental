# -*- encoding: utf-8 -*-
# stub: rbczmq 1.7.9 ruby lib
# stub: ext/rbczmq/extconf.rb

Gem::Specification.new do |s|
  s.name = "rbczmq".freeze
  s.version = "1.7.9"

  s.required_rubygems_version = Gem::Requirement.new(">= 0".freeze) if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib".freeze]
  s.authors = ["Lourens Naud\u00E9".freeze, "James Tucker".freeze, "Matt Connolly".freeze]
  s.date = "2015-07-03"
  s.description = "Ruby extension for ZeroMQ (ZMQ) using CZMQ - High-level C Binding for \u00D8MQ (http://czmq.zeromq.org)".freeze
  s.email = ["lourens@methodmissing.com".freeze, "jftucker@gmail.com".freeze, "matt.connolly@me.com".freeze]
  s.extensions = ["ext/rbczmq/extconf.rb".freeze]
  s.files = ["ext/rbczmq/extconf.rb".freeze]
  s.homepage = "http://github.com/methodmissing/rbczmq".freeze
  s.licenses = ["MIT".freeze]
  s.rdoc_options = ["--charset=UTF-8".freeze]
  s.rubygems_version = "2.7.7".freeze
  s.summary = "Ruby extension for ZeroMQ (ZMQ) using CZMQ - High-level C Binding for \u00D8MQ (http://czmq.zeromq.org)".freeze

  s.installed_by_version = "2.7.7" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_development_dependency(%q<minitest>.freeze, ["~> 5.5.0"])
      s.add_development_dependency(%q<rake-compiler>.freeze, ["~> 0.8.0"])
    else
      s.add_dependency(%q<minitest>.freeze, ["~> 5.5.0"])
      s.add_dependency(%q<rake-compiler>.freeze, ["~> 0.8.0"])
    end
  else
    s.add_dependency(%q<minitest>.freeze, ["~> 5.5.0"])
    s.add_dependency(%q<rake-compiler>.freeze, ["~> 0.8.0"])
  end
end
