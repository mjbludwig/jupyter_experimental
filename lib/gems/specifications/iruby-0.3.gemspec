# -*- encoding: utf-8 -*-
# stub: iruby 0.3 ruby lib

Gem::Specification.new do |s|
  s.name = "iruby".freeze
  s.version = "0.3"

  s.required_rubygems_version = Gem::Requirement.new(">= 0".freeze) if s.respond_to? :required_rubygems_version=
  s.require_paths = ["lib".freeze]
  s.authors = ["Daniel Mendler".freeze, "The SciRuby developers".freeze]
  s.date = "2017-03-26"
  s.description = "A Ruby kernel for Jupyter/IPython frontends (e.g. notebook). Try it at try.jupyter.org.".freeze
  s.email = ["mail@daniel-mendler.de".freeze]
  s.executables = ["iruby".freeze]
  s.files = ["bin/iruby".freeze]
  s.homepage = "https://github.com/SciRuby/iruby".freeze
  s.licenses = ["MIT".freeze]
  s.post_install_message = "Consider installing the optional dependencies to get additional functionality:\n  * pry\n  * pry-doc\n  * awesome_print\n  * gnuplot\n  * rubyvis\n  * nyaplot\n\n".freeze
  s.required_ruby_version = Gem::Requirement.new(">= 2.1.0".freeze)
  s.rubygems_version = "2.7.7".freeze
  s.summary = "Ruby Kernel for Jupyter/IPython".freeze

  s.installed_by_version = "2.7.7" if s.respond_to? :installed_by_version

  if s.respond_to? :specification_version then
    s.specification_version = 4

    if Gem::Version.new(Gem::VERSION) >= Gem::Version.new('1.2.0') then
      s.add_development_dependency(%q<rake>.freeze, ["~> 10.4"])
      s.add_development_dependency(%q<minitest>.freeze, ["~> 5.6"])
      s.add_runtime_dependency(%q<bond>.freeze, ["~> 0.5"])
      s.add_runtime_dependency(%q<multi_json>.freeze, ["~> 1.11"])
      s.add_runtime_dependency(%q<mimemagic>.freeze, ["~> 0.3"])
      s.add_runtime_dependency(%q<data_uri>.freeze, ["~> 0.1"])
    else
      s.add_dependency(%q<rake>.freeze, ["~> 10.4"])
      s.add_dependency(%q<minitest>.freeze, ["~> 5.6"])
      s.add_dependency(%q<bond>.freeze, ["~> 0.5"])
      s.add_dependency(%q<multi_json>.freeze, ["~> 1.11"])
      s.add_dependency(%q<mimemagic>.freeze, ["~> 0.3"])
      s.add_dependency(%q<data_uri>.freeze, ["~> 0.1"])
    end
  else
    s.add_dependency(%q<rake>.freeze, ["~> 10.4"])
    s.add_dependency(%q<minitest>.freeze, ["~> 5.6"])
    s.add_dependency(%q<bond>.freeze, ["~> 0.5"])
    s.add_dependency(%q<multi_json>.freeze, ["~> 1.11"])
    s.add_dependency(%q<mimemagic>.freeze, ["~> 0.3"])
    s.add_dependency(%q<data_uri>.freeze, ["~> 0.1"])
  end
end
