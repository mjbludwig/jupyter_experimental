Disclaimer:

This Jupyter notebook app is based off of the one generously shared by the folks at the OSC. See it here: https://github.com/OSC/bc_osc_jupyter See their README labeled OSC_README.md. 

#### Job of this app

Users of this cluster/ood have various workflows revolving around conda and varying levels of skill. The other tricky thing is this cluster has IBM's Watson (WMLCE) support which is best when run inside a conda env.

+ This app defaults to just using the vanilla anaconda module installed on the cluster, starting jupyter right from the base environment, i.e. not activating a specific one.

+ A user can then decide that they want to instead use the anaconda module that has some of the WMLCE stuff added (IBM conda channel, some of the powerai packages).

+ If a user is a little more advanced, they can instead enter the path to a custom anaconda install they have, again this would just run out of the base env.

+ Finally, using some combination of the above, a user can specify a specific path to a conda environment. They need to be wise enough to know what conda module was used to build their environment and load the respective one.

All of these options change dynamically with the Javascript. We inherently trust our users so one will notice the lax (non existant) security checking in it. I am fully aware that users _could_ probably inject commands, etc but this all runs under their user anyways to w/e... 


#### Assumptions and requisites:

+ This cluster is running lsf.
+ There are 2x global anaconda3 modules, named `anaconda3/2019.10` and `anaconda3/2019.10_powerai`.
+ There is a globally reachable space for OOD specific modules
+ Morgan is not great at writing Ruby or Javascript...

#### Redactions

I have scrubbed identifying elements in this app like paths, cluster name and other identifying information. The removed areas are replaced with `-- REDACTED --`.


If you have any questions or comments about this app, feel free to reach out to me at mludwig@techsquare.com
